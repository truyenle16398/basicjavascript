let bill = [
    {
        ID: 1,
        content: [{ productID: 325, amount: 2 }, { productID: 612, amount: 6 }, { productID: 123, amount: 10 }],
        totalPrice: 0
    },
    {
        ID: 2,
        content: [{ productID: 601, amount: 1 }, { productID: 123, amount: 2 }, { productID: 325, amount: 3 }],
        totalPrice: 0
    },
];
let product = [
    {
        ID: 123,
        name: "The New iPad 2018",
        price: 799,
        brand: "Apple",
        classify: "Tablet"
    },
    {
        ID: 325,
        name: "Mi Mix 3",
        price: 499,
        brand: "Xiaomi",
        classify: "Smartphone"
    },
    {
        ID: 612,
        name: "Alienware 17R5",
        price: 1549,
        brand: "Dell",
        classify: "Laptop"
    },
    {
        ID: 601,
        name: "Macbook Pro 2018",
        price: 1299,
        brand: "Apple",
        classify: "Laptop"
    }
];

//bài 1
bill.forEach((element) => {//duyệt mảng bill để làm việc với obj trong content
    let sum = 0;
    element.content.forEach((element1) => {//duyệt mảng bill xong duyệt mảng content để làm việc với productID và amount
        product.forEach((element2) => {//duyệt mảng product để check ID là làm việc với price
            if (element1.productID === element2.ID) {//Check ID. Nếu content[i].productID trong mảng bill = product[i].ID.
                let temp = element1.amount * element2.price;//thì lấy content[i].amount của nó * với product[i].price. Cái nào khớp ID thì cộng, ko thì bỏ qua 
                sum = sum + temp;//ban đầu cho sum=0, sau mỗi lần check ID khớp thì lấy amount*price sau đó cộng dồn vào sum
            }
        });
    });
    element.totalPrice = sum;//lấy kết quả sum cuối cùng gán cho totalPrice
});

//bài 2
function findBrand(brand) {
    let obj = [];
    product.forEach((element) => {//duyệt mảng product để làm việc với product[i].brand
        if (brand === element.brand) {//so sánh product[i].brand với biến brand tạo ở trong function
            obj.push(element);//thêm vào mảng obj những sản phẩm tìm được (trùng brand)
        }
    });
    return obj;
}

findBrand("Apple").forEach((element) => {//in ra tất cả các sản phẩm tìm được
    console.log(element);
});

//bài 3
function displayBill(indexID) {//indexID là để check với số ID của bill
    let displayBill;//displayBill là chuỗi cuối cùng cần hiển thị
    bill.forEach((element) => {//duyệt mảng bill để làm việc với bill[i].ID
        let str = ``;
        if (indexID === element.ID) {//Nếu ID nhập vào có trong mảng bill
            element.content.forEach((element1, index) => {//duyệt mảng content để làm việc với productID và amount
                product.forEach((element2) => {//duyệt mảng product để làm việc với tất cả các thuộc tính
                    if (element1.productID === element2.ID) {//check ID của product với productID để tính tiền theo đúng sản phẩm trong bill
                        let temp = `${index + 1}. ${element2.brand} ${element2.name} x${element1.amount}: ${element1.amount * element2.price} USD`;
                        //tính tiền và in ra theo từng sẩn phẩm 
                        str = `${str}\n ${temp}`;// cộng vào chuỗi str để xí nữa hiển thị hết tất cả các sản phẩm trong product
                    }
                });
            });
            displayBill = `${str} \n\tTotal Price: ${element.totalPrice} USD`//cuối cùng trả về str + totalPrice đã tính và update dât ở bài 1
        }
    });
    return displayBill;
}
console.log(displayBill(1));
