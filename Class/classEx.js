class Personal {
    constructor(ID, fullName, position, seniority, birthday = {}) {
        this.ID = ID;
        this.fullName = fullName;
        this.position = position;
        this.seniority = seniority;
        this.birthday = birthday;
    }

}