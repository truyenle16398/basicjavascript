class Customer{
    constructor(name,member,type){
        this.name = name;
        this.member = member;
        this.type = type;
    }
};

class DiscountRate{
    constructor(serviceDiscount,productDiscount){
        this.serviceDiscount = serviceDiscount;
        this.productDiscount = productDiscount;
    }
}