class Animal {
    constructor(name, age, spec) {//hàm khởi tạo giá trị ban đầu
        this.name = name;
        this.age = age;
        this.spec = spec;
    }

    get getAge() {
        return this.age;
    }

    static sumCatAge(cat1, cat2) {//gọi qua Animal.sumCatAge, ko gọi qua cat1.sumCatAge
        return cat1.age + cat2.age;
    }
};

class Dog extends Animal {
    constructor(name, age, gender, spec = "dog") {
        super(name, age, spec);
        this.gender = gender;
    }

    speak() {
        return "Gau gau";
    }
}

let cat1 = new Animal("Paw", 8, "cat");
let cat2 = new Animal("Pew", 10, "cat");//cat1, cat2 là instance của class Animal
let dog1 = new Animal("thành", 5,"male");
console.log(dog1);

console.log(cat1);
//Check cat1
cat1 instanceof Animal
//Call method speak()
console.log(cat1.getAge);//hàm get là hàm đặc biệt, ko cần ()
console.log(Animal.sumCatAge(cat1, cat2));

Animal.prototype.color = "black";
console.log(cat1.color);
console.log(cat1.hasOwnProperty("color"));


