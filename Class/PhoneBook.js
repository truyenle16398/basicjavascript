class Contact {
    constructor(id, name, phoneNumber){
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }
}

class PhoneBook {
    constructor(phoneBookID, listContact = []) {
        this.phoneBookID = phoneBookID;
        this.listContact = listContact;
    }
    insert(id,name, number) {
        return this.listContact.push(new Contact(id,name,number));
    }
    lookUp(str) {
        let arr = []
        this.listContact.forEach((element) => {
            if (element.name.includes(str) == true || element.phoneNumber.includes(str) == true) {
                arr.push(element);
            }
        });
        return arr;
    }
    update(id, name, number) {
        this.listContact.forEach((element) => {
            if (id === element.id) {
                element.name = name;
                element.phoneNumber = number;
            }
        });
    }
    delete(id){
        this.listContact.forEach((element) => {
            if (id === element.id) this.listContact.splice(element.id - 1,1);
        });
    }
}

let PhoneBook1 = new PhoneBook(1);
PhoneBook1.insert(1,"truyen", "123");
PhoneBook1.insert(2,"truyen1", "456");
PhoneBook1.insert(3,"truyen2", "567");
PhoneBook1.insert(4,"thanh2", "891");

// PhoneBook1.lookUp("2");
PhoneBook1.update(2,"edit","edit");
PhoneBook1.delete(3);
console.log(PhoneBook1);
