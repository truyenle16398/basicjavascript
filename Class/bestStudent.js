class Student {
    constructor(ID, firstName, lastName, scores = {}) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.scores = scores;
    }

    get averageScore() {
        let sum = 0, count = 0;
        Object.values(this.scores).forEach((element, index) => {
            sum += element;
            count = index + 1;
        });;
        return sum / count;
    }

    get studentTitle() {
        let str = ""
        switch (true) {//hàm switch này để xác định danh hiệu học sinh theo điểm TB trước.
            case this.averageScore < 5: str = "Bad Student";
            case this.averageScore >= 5 && this.averageScore < 6.5: str = "Average Student";
            case this.averageScore >= 6.5 && this.averageScore < 8: str = "Good Student";
            case this.averageScore >= 8: str = "Excellent Student";
        }
        Object.values(this.scores).forEach((element) => {
            element < 3.5 ? str = "Bad Student" : element < 5 ? str = "Average Student" : element < 6.5 ? str = "Good Student" : str = str;
        });
        return str;
    }

    static bestStudent(arr) {
        return arr.sort((a, b) => a.averageScore < b.averageScore)// sắp xếp mảng được truyền vào 
            .splice(0, Math.ceil(arr.length / 10));//lấy ra 10% phần tử, bắt đầu từ pt 0
    }

}


let student1 = new Student(1, "Le Huy", "Truyen", { math: 3.4, physical: 10, chemistry: 10, theduc: 10 });
let student2 = new Student(2, "Ngo Ba", "Kha", { math: 10, physical: 9, chemistry: 9 });
let student3 = new Student(3, "Duong Minh", "Tuyen", { math: 10, physical: 8, chemistry: 10 });
let student4 = new Student(4, "Phu", "Le", { math: 6, physical: 6, chemistry: 6 });

let arr1 = [student1, student2, student3, student4];

console.log(`Điểm tb: ${student1.averageScore} có danh hiệu: ${student1.studentTitle}`);

console.log(Student.bestStudent(arr1));
