let Scores = [
    {
        studentName: "Phan Van Teo1",
        math: 0.7,
        physical: 0,
        chemistry: 0,
        literature: 0,
        history: 0,
        geography: 0
    },
    {
        studentName: "Phan Van Teo",
        math: 8.7,
        physical: 7.2,
        chemistry: 6.9,
        literature: 7.8,
        history: 7.6,
        geography: 9.1
    },
    {
        studentName: "Ban Thi No",
        math: 9.2,
        physical: 6.5,
        chemistry: 7.0,
        literature: 6.3,
        history: 5.6,
        geography: 8.5
    },
    {
        studentName: "Ton That Hoc",
        math: 1.2,
        physical: 5.7,
        chemistry: 2.1,
        literature: 10,
        history: 9.5,
        geography: 9.8
    },
    {
        studentName: "Phan Van Teo2",
        math: 10,
        physical: 10,
        chemistry: 10,
        literature: 10,
        history: 10,
        geography: 10
    }
];
let avg;
let max = 0;
let bestStudent;
// Cách 1: for(i)
// for (let i = 0; i < Scores.length; i++) {
//     avg = (Scores[i].math + Scores[i].physical + Scores[i].chemistry + Scores[i].literature + Scores[i].history + Scores[i].geography) / 6;// tính điểm trung bình
//     Scores[i].avg = avg;//thêm vào object thuộc tính avg, có values là avg vừa mới tính
//     if (Scores[i].avg > max) {
//         max = Scores[i].avg;
//         bestStudent = Scores[i];
//     }
// }
//Cách 2:forEach
Scores.forEach((element,index)=> {
    let Avg = (element.math + element.physical + element.chemistry + element.literature + element.history + element.geography)/6;
    element.Avg = Avg; 
    console.log(element.Avg);
    if (element.Avg > max) {
        max = element.Avg;
        bestStudent = element;
    }    
});


console.log(`best student is: ${bestStudent.studentName}. Avg Scores = ${bestStudent.Avg}`);
