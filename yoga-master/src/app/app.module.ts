import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { CardComponent } from './card/card.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventCardComponent } from './event-card/event-card.component';
import * as firebase from 'firebase';
import { AuthService } from './service/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    LoginComponent,
    HeaderComponent,
    CardComponent,
    RegisterComponent,
    EventCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    var firebaseConfig = {
      apiKey: "AIzaSyBXh7nX3we8p8UdbjjbVhLTP_5WPzOV3D4",
      authDomain: "test-54c7e.firebaseapp.com",
      databaseURL: "https://test-54c7e.firebaseio.com",
      projectId: "test-54c7e",
      storageBucket: "test-54c7e.appspot.com",
      messagingSenderId: "1074069205162",
      appId: "1:1074069205162:web:743ebe414fc2b834"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
