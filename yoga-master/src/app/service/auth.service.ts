import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  onRegister(user) {
    firebase.auth().createUserWithEmailAndPassword(user.username, user.password);
  }
}
