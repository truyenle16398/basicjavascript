let customer = {
    firstName: "John",
    lastName: "Smith",
    dateOfBirth: { day: 6, month: 2, year: 1998 },
    gender: "male",
    phoneNumber: "0775842390",
    email: "john_smith@gmail.com",
    address: "13th Street 47W, New York City"
};

//C1:
// let arr = Object.entries(customer);
// let arrObject = [];
// for (let i = 0; i < arr.length; i++) {
//     if (arr[i][0] === `dateOfBirth`) {
//         let obj = { title: arr[i][0], value: `${arr[i][1].day}/${arr[i][1].month}/${arr[i][1].year}` };
//         arrObject.push(obj);
//     }
//     else {
//         let obj = { title: arr[i][0], value: arr[i][1] };
//         arrObject.push(obj);
//     }
// }
// console.log(JSON.stringify(arrObject));

//C2
// let arrKeys = Object.keys(customer);
// let arrValues = Object.values(customer);
// let arrObject = [];
// for (let i = 0; i < arrKeys.length; i++) {
//     if (arrKeys[i] === `dateOfBirth`) {
//         let obj1 = {title: arrKeys[i], value: `${arrValues[i].day}/${arrValues[i].month}/${arrValues[i].year}`};
//         arrObject.push(obj1);
//     } else {
//         let obj = {title: arrKeys[i], value: arrValues[i]};
//       arrObject.push(obj);
//     }
// }
// console.log(JSON.stringify(arrObject));

//C3
let result = Object.keys(customer).map(key =>{
    if (key === 'dateOfBirth') return {title: key, value: `${customer[key].day}/${customer[key].month}/${customer[key].year}`};
    return {title: key, value: customer[key]};
})
console.log(JSON.stringify(result));
