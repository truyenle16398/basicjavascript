//truyền dữ liệu từ ngoài vào trong object
/*let name = 'ABC';
let age = 18;
let object = {name, age}; */

// truyền(transfer) dữ liệu từ trong object ra ngoài
let object = { name: 'Truyen', age: 18 };
let { name, age } = object;

let object2 = Object.assign({}, object);// khởi tạo 1 object2. cùng dữ liệu mà khác bộ nhớ

console.log(object2);

//kiểm tra 'name' có ở trong object2 hay ko
console.log('name' in object2);//cách 1
console.log(object2.age != undefined);//cách 2

//get keys from object
console.log(Object.keys(object2));//get keys
console.log(Object.values(object2));//get values

Object.values(object2).forEach(values => {//chạy từ đầu tới cuối mảng rồi ghi vào element(values)
    console.log(`${values} asdasd`);
});

delete object2.birth; //xóa 'name' trong object

//convert object to JSON
let json = JSON.stringify(object2);
//convert JSON to object
object2 = JSON.parse(json);
